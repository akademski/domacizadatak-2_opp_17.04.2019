﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomaciZadatak2_opp_17._04._2019
{
    class Automobil:Vozilo
    {
        private int brojSedista;
        private int brojVrata;

        public Automobil(string marka, string model, int snaga, double prosecnaPotrosnja
            , double godinaProizvodnje, double cena, int brojSedista, int brojVrata)
            : base( marka, model, snaga, prosecnaPotrosnja, godinaProizvodnje, cena)
        {
            this.brojSedista = brojSedista;
            this.brojVrata = brojVrata;
        }

        public override double OcenaVozila()
        {
            return base.OcenaVozila()+(10-this.prosecnaPotrosnja);
        }

        public override void Prikaz()
        {
            Console.WriteLine($"Auto je marke {this.marka}, model {this.model}" +
                $", snage motora {this.snaga} kW, prosecne potrosnje {this.prosecnaPotrosnja} litara" +
                $", godine proizvodnje {this.godinaProizvodnje}, cene {this.cena} eura" +
                $", broj sedista {this.brojSedista}"+
                $", broj vrata {this.brojVrata}"+
                $". Ukupna ocena vozila je {this.OcenaVozila()}");
        }

    }
}
