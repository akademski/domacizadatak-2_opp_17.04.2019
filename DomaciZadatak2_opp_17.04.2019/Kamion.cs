﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomaciZadatak2_opp_17._04._2019
{
    class Kamion:Vozilo
    {
        private double visina;
        private double nosivost;

        public Kamion(string marka, string model, int snaga, double prosecnaPotrosnja
            , double godinaProizvodnje, double cena, double visina, double nosivost)
            : base(marka, model, snaga, prosecnaPotrosnja, godinaProizvodnje, cena)
        {
            this.visina = visina;
            this.nosivost = nosivost;
        }

        public override double OcenaVozila()
        {
            return base.OcenaVozila()+ this.nosivost/2;
        }

        public override void Prikaz()
        {
            Console.WriteLine($"Kamion je marke {this.marka}, model {this.model}" +
                $", snage motora {this.snaga} kW, prosecne potrosnje {this.prosecnaPotrosnja} litara" +
                $", godine proizvodnje {this.godinaProizvodnje}, cene {this.cena} eura" +
                $", visine {this.visina} i"+
                $" nosivosti {this.nosivost} tona"+
                $". Ukupna ocena vozila je {this.OcenaVozila()}");
        }
    }
}
