﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomaciZadatak2_opp_17._04._2019
{
    class Motocikl:Vozilo
    {
        private ModMotora modMotora;

        public Motocikl(string marka, string model, int snaga, double prosecnaPotrosnja
            , double godinaProizvodnje, double cena, ModMotora modMotora)
            : base(marka, model, snaga, prosecnaPotrosnja, godinaProizvodnje, cena)
        {
            this.modMotora = modMotora;
        }

        public override double OcenaVozila()
        {
            return base.OcenaVozila();
        }

        public override void Prikaz()
        {
            Console.WriteLine($"Motocikl je marke {this.marka}, model {this.model}" +
                $", snage motora {this.snaga} kW, prosecne potrosnje {this.prosecnaPotrosnja} litara" +
                $", godine proizvodnje {this.godinaProizvodnje}, cene {this.cena} eura" +
                $", tip Motora: {this.modMotora}" +
                $". Ukupna ocena vozila je {this.OcenaVozila()}");
        }
    }
}
