﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomaciZadatak2_opp_17._04._2019
{
    class Program
    {
        static void Main(string[] args)
        {
            Vozilo[] vozila;
            double suma=0;
            int indexNajboljaOcena=0;

            
            // verzija kada se direktno zadaju parametri
            //vozila[0] = new Automobil("Toyota", "Yaris", 200, 4.5, 2010, 100000, 5, 4);
            //vozila[1] = new Kamion("Mercedes", "Atego", 500, 10.3, 2008, 440000, 3.2, 5.5);
            //vozila[2] = new Motocikl("Yamaha", "YZF-R1M", 440, 3, 2018, 80000, ModMotora.SPORT);
            //vozila[3] = new Automobil("Nissan", "Micra", 250, 5.5, 2015, 150000, 4, 2);

            // verzija kada se dinamicki vrsi unos podataka
            string marka;
            string model;     
            int snaga;
            double prosecnaPotrosnja;
            double godinaProizvodnje; 
            double cena;
            int counter = 0;
            int i = 0;
           
            Console.WriteLine("Unesite broj vozila");
            //int brojVozila;
            int.TryParse(Console.ReadLine(), out int brojVozila);
            vozila = new Vozilo[brojVozila];

            Console.WriteLine("Meni: 0- izlaz,  bilo koji drugi broj za unos podataka o vozilu");
            int opcija = int.Parse(Console.ReadLine());
            while (opcija != 0 )
            {
                
                Console.WriteLine("Izbor vozila: 1- automobil 2- kamion 3- motocikl");
                int opcijaVozilo = int.Parse(Console.ReadLine());
                

                Console.WriteLine("Unesite marku vozila");
                marka = Console.ReadLine();
                Console.WriteLine("Unesite model");
                model = Console.ReadLine();
                Console.WriteLine("Unesite snagu motora");
                snaga = int.Parse(Console.ReadLine());
                Console.WriteLine("Unesite prosecnu potrosnju");
                prosecnaPotrosnja = double.Parse(Console.ReadLine());
                Console.WriteLine("Unesite godinu proizvodnje");
                godinaProizvodnje = double.Parse(Console.ReadLine());
                Console.WriteLine("Unesite cenu");
                cena = double.Parse(Console.ReadLine());

                switch (opcijaVozilo)
                {
                    case 1:
                        Console.WriteLine("Unesite broj sedista");
                        int brojSedista = int.Parse(Console.ReadLine());
                        Console.WriteLine("Unesite broj vrata");
                        int brojVrata = int.Parse(Console.ReadLine());
                        vozila[i] = new Automobil(marka,model,snaga, prosecnaPotrosnja, godinaProizvodnje, cena, brojSedista, brojVrata);
                        i++;
                        break;
                    case 2:
                        Console.WriteLine("Unesite visinu kamiona");
                        double visina = double.Parse(Console.ReadLine());
                        Console.WriteLine("Unesite nosivost kamiona");
                        double nosivost = double.Parse(Console.ReadLine());
                        vozila[i] = new Kamion(marka, model, snaga, prosecnaPotrosnja, godinaProizvodnje, cena, visina, nosivost);
                        i++;
                        break;
                    case 3:  
                        Console.WriteLine("Unesite tip motora: 1- MOPED 2- CRUISER 3- SCOOTER 4- SPORT");
                        ModMotora modMotora = (ModMotora)int.Parse(Console.ReadLine());
                        vozila[i] = new Motocikl(marka, model, snaga, prosecnaPotrosnja, godinaProizvodnje, cena, modMotora);
                        i++;
                        break;
                }

                counter++;
                if (brojVozila <=counter)
                {
                    Console.WriteLine("Na lageru ima {0} vozila", counter);
                    break;
                }

                if (counter >= 100)
                {
                    Console.WriteLine("Dostigunt je maksimlni broj vozila koji se mogu uneti");
                    Console.WriteLine("Press any key...");
                    Console.ReadKey();
                    Console.Clear();
                    break;
                }

                Console.WriteLine("Meni: 0- izlaz,  bilo koji drugi broj za unos podataka o vozilu");
                int.TryParse(Console.ReadLine(), out opcija);
             
            }
 
            Console.WriteLine("Lista dostupnih vozila:");
            for (int j = 0; j < counter; j++)
            {
                if (vozila[j] == null)
                {
                    break;
                }
                
                vozila[j].Prikaz();
                
                
                suma += vozila[j].Cena;

                if (vozila[0].OcenaVozila() < vozila[j].OcenaVozila())
                {
                    indexNajboljaOcena = j;
                }
                Console.WriteLine();
            }
            
            // ako nije izabrana 0 u prvom odabiru
            if (vozila[0] != null)
            {

                Console.WriteLine("Ukupna vrednost svih vozila je {0}", suma);
                
                Console.WriteLine("Vozilo sa najboljom ocenom je: {0}", vozila[indexNajboljaOcena].GetType().Name);
                vozila[indexNajboljaOcena].Prikaz();
            }
           
            else
            {
                Console.WriteLine("Nema vozila na stanju");
            }
            
        }
    }
}
