﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomaciZadatak2_opp_17._04._2019
{
    class Vozilo
    {
        protected string marka;
        protected string model;
        protected int snaga;
        protected double prosecnaPotrosnja;
        protected double godinaProizvodnje;
        protected double cena;
        public double Cena
        {
            get
            {
                return this.cena;
            }
        }

        public Vozilo(string marka, string model, int snaga, double prosecnaPotrosnja
            , double godinaProizvodnje, double cena)
        {
            this.marka = marka;
            this.model = model;
            this.snaga = snaga;
            this.prosecnaPotrosnja = prosecnaPotrosnja;
            this.godinaProizvodnje = godinaProizvodnje;
            this.cena = cena;
        }

        public virtual double OcenaVozila()
        {
            double starost = DateTime.Now.Year - this.godinaProizvodnje;
            double ocena = (5 - Cena/ 10000) + (5 - starost / 5);
            return ocena;
        }

        public virtual void Prikaz()
        {
            Console.WriteLine($"Vozilo je marke {this.marka}, model {this.model}" +
                $", snage motora {this.snaga} kW, prosecne potrosnje {this.prosecnaPotrosnja} litara" +
                $", godina proizvodnje {this.godinaProizvodnje}, i cene {this.cena} eura" +
                $". Ukupna ocena vozila je {this.OcenaVozila()}");
        }

       
    }
}
